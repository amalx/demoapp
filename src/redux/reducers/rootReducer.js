import {combineReducers} from 'redux'
import auth from './authReducer'
import employeesReducer from './employeesReducer'

const rootReducer = combineReducers({
    auth,
    employeesReducer
})

export default rootReducer