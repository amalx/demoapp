const INITIAL_STATE={
  isLoggedIn:false,
  error:undefined
}

export default function auth(state=INITIAL_STATE,action){
    
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return{
              ...state,
              isLoggedIn:true,
            }
        case 'LOGIN_FAILED':
            return{
              ...state,
              isLoggedIn:false,
            }
        break;
        case 'LOGOUT':
            return{
              ...state,
              isLoggedIn:false
            }
        break;
        default:
            return state
  }
}