const userCredentials = {
    username:"hruday@gmail.com",
    password :"hruday123"
}

export function loginSuccess(userData){
    return{
        type:'LOGIN_SUCCESS'
    }
}

export function loginFailed(error){
    console.log("inside login failed")
    return{
        type:'LOGIN_FAILED'
    }
}

export function login(data){
    console.log(data)
    return dispatch => { 
        if(data.email == userCredentials.username && data.password == userCredentials.password)
            dispatch(loginSuccess())
        else
            dispatch(loginFailed())
    }
}
  
  