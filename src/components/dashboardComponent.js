import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, FlatList } from 'react-native'
import {connect} from 'react-redux'
import * as actions from '../redux/actions/employeeAction'

class Dashboard extends React.Component {

    constructor (props) 
    {
        super(props)
    }

    static navigationOptions = {
        title: 'EMPLOYEES',
        headerLeft: null,
        headerTitleStyle: {
            flex:1,
            color: '#00005A',
            fontWeight: '600',
            textAlign:"center",
        },
    }

    render()
    {
        return (
            <FlatList
                style={{ marginTop: 40}}
                data={this.props.employees}
                renderItem={
                    ({item}) => <View style={{flex:1,marginBottom:30,justifyContent:'center',alignItems:'center'}}>
                                    <View style={styles.list}>        
                                        <Text style={styles.nameText}>{item.name}</Text>
                                        <View style={styles.subContent}>
                                            <Text style={styles.otherText}>Email : {item.email}</Text>
                                            <Text style={styles.otherText}>Phone : {item.phoneNo}</Text>
                                            <Text style={styles.otherText}>Gender : {item.gender}</Text>
                                            <Text style={styles.otherText}>Age : {item.age}</Text>
                                        </View>
                                    </View>
                                </View>}

            />
        );
    }

    componentDidMount()
    {
        this.props.fetchEmployees()
    }
  
}

const mapStateToProps = state => ({
    error: state.employeesReducer.error,
    employees: state.employeesReducer.employees,
})

const mapDispatchToProps = dispatch => ({
    fetchEmployees:() => dispatch(actions.fetchEmployees())
})

export default connect(mapStateToProps,mapDispatchToProps)(Dashboard)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    list: {
        flex:1, 
        width: '90%', 
        backgroundColor: '#1173b8',
        borderRadius: 5,
        borderWidth:1,
        borderColor: '#b5e2fd', 
        padding: 10
    },
    nameText: {
        flex: 1,
        fontWeight: 'bold',
        fontSize: 16,
        textTransform: 'uppercase',
        color:'#ffff'
    },
    otherText: {
        fontSize: 14,
        color:'#00005A'
    },
    subContent: {
        flex: 3,
        backgroundColor: '#ffff',
        borderRadius: 5,
        padding: 5
    }
  
});
