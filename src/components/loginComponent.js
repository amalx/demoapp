import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import {connect} from 'react-redux'
import * as actions from '../redux/actions/authAction'
import NavigationService from '../services/navigationService'
import { showMessage, hideMessage } from "react-native-flash-message"

validateEmail = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ 
    if(reg.test(text) === false)
        return false
    else 
        return true
}

class Login extends React.Component {

    constructor (props) 
    {
        super(props)
        this.state = {
            email: '',
            password:''
        }
        this.login = this.login.bind(this)
    }
    
    async login(){

        if(this.state.email == '' || this.state.password == '')
        {
            showMessage({
                message: "Email and Password are mandatory",
                type: "warning",
            })
            return
        }       

        let emailValidation = await validateEmail(this.state.email)
        if(!emailValidation)
        {
            showMessage({
                message: "Check Your Email",
                type: "warning",
            })
            return
        }

        let login = await this.props.login(this.state.email,this.state.password)
        if(this.props.isLoggedIn)
            NavigationService.navigate('dashboard')
        else
            showMessage({
                message: "User Not Found",
                type: "warning",
            });  
    }

    render()
    {
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                
                <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
                    <View style={styles.drawCircle}>
                        <Text style={styles.logoText}>VX</Text>
                    </View>
                </View>
                <View style={{flex:4,alignItems:'center'}}>
                    <View style={styles.inputView}>
                        <Image style={styles.inputImage} source={require('../images/icons/username_field_icon2x.png')}/>
                        <TextInput
                        placeholder = 'Email'
                        placeholderTextColor="#2b3164" 
                        autoCapitalize = 'none'
                        onChangeText={(email) => this.setState({email})}
                        style={styles.input}                        
                        />
                    </View>
                    <View style={styles.inputView}>
                        <Image style={styles.inputImage} source={require('../images/icons/password_field_icon2x.png')}/>
                        <TextInput
                        secureTextEntry={true}
                        placeholder = 'Password'
                        placeholderTextColor="#2b3164"
                        autoCapitalize = 'none' 
                        onChangeText={(password) => this.setState({password})}
                        style={styles.input}                        
                        />
                    </View>
                        
                    <TouchableOpacity onPress={this.login} style={styles.button}>
                        <Text style={{color:'white'}}>LOGIN</Text>
                    </TouchableOpacity>      
                </View>
                
            </KeyboardAvoidingView>
        );
    }
  
}

const mapStateToProps = state => ({
    isLoggedIn:state.auth.isLoggedIn,
    userData:state.auth.userData,
    error:state.auth.error
})

const mapDispatchToProps = dispatch => ({
    login:(email,password) => dispatch(actions.login({email,password}))
})

export default connect(mapStateToProps,mapDispatchToProps)(Login)


const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff'
  },
  input: {
      flex:1,
      paddingLeft:10,
      color:'#2b3164',
      height:'100%',
      width: '100%'
  },
  inputImage: {
      width: 18, 
      height:18
  },
  inputView: {
      flexDirection: 'row',
      alignItems:'center',
      height: 45,
      paddingLeft:5, 
      width:'80%', 
      marginBottom:40,
      backgroundColor:'#b5e2fd',
      borderRadius:25
  },
  button: {
      alignItems: 'center',
      justifyContent:'center',
      width:'80%',
      height:40,
      borderRadius:25,
      backgroundColor:'#1173b8'
  },
  drawCircle: {
      borderWidth: 1,
      borderRadius: 45, 
      borderColor: '#1173b8',
      padding:20
  },
  logoText: {
      color:'#1173b8', 
      fontSize:38
  }
});
